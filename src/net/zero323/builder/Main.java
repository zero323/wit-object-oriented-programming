package net.zero323.builder;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {
		ArrayList<Token> tokens = Main.getTokens();
		Director dir = new Director();
		Builder builder = new TextBuilder();
		dir.setBuilder(builder);
		dir.constructDocument(tokens);
		
		try {
			FileWriter textWriter = new FileWriter("/home/zero323/Tmp/out.txt");
			textWriter.write(dir.getDocument().getContent());
			textWriter.close();
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		
		builder = new HtmlBuilder();
		dir.setBuilder(builder);
		dir.constructDocument(tokens);
		System.out.println(dir.getDocument().getContent());
		
		try {
			FileWriter htmlWriter = new FileWriter("/home/zero323/Tmp/out.html");
			htmlWriter.write(dir.getDocument().getContent());
			htmlWriter.close();
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	private static ArrayList<Token>getTokens() {

		ArrayList<Token> tokens = new  ArrayList<Token>();
		
		tokens.add(new Token(TokenTypes.TITLE, "Tytuł dokumentu"));
		tokens.add(new Token(TokenTypes.AUTHOR, "Piotr Kowalski"));
		tokens.add(new Token(TokenTypes.AUTHOR, "Ewa Józefowicz"));
		tokens.add(new Token(TokenTypes.HEADER_H1, "Przykładowy Dokument"));
		tokens.add(new Token(TokenTypes.HEADER_H2, "Paragraf pierwszy"));
		tokens.add(new Token(TokenTypes.PARGARPH, "Lorem ipsum dolor sit amet, "+
					"consectetur adipisicing elit, sed do eiusmod tempor incididunt " +
					"ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis " + "" +
					"nostrud exercitation ullamco laboris nisi ut aliquip ex ea " + "" +
					"commodo consequat. Duis aute irure dolor in reprehenderit in " + 
					"voluptate velit esse cillum dolore eu fugiat nulla pariatur. " +
					"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui " + 
					"officia deserunt mollit anim id est laborum"));
		tokens.add(new Token(TokenTypes.HEADER_H2, "Paragraf drugi"));
		tokens.add(new Token(TokenTypes.PARGARPH, "Lorem ipsum dolor sit amet, "+
					"consectetur adipisicing elit, sed do eiusmod tempor incididunt " +
					"ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis " + "" +
					"nostrud exercitation ullamco laboris nisi ut aliquip ex ea " + "" +
					"commodo consequat. Duis aute irure dolor in reprehenderit in " + 
					"voluptate velit esse cillum dolore eu fugiat nulla pariatur. " +
					"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui " + 
					"officia deserunt mollit anim id est laborum"));
		tokens.add(new Token(TokenTypes.HEADER_H3, "Lista"));
		tokens.add(new Token(TokenTypes.LIST, "Pierwszy punkt \n"+
					"Drugi punkt \n" +
					"Trzeci punkt \n" ));
		tokens.add(new Token(TokenTypes.HEADER_H2, "Paragraf trzeci"));
		tokens.add(new Token(TokenTypes.PARGARPH, "Lorem ipsum dolor sit amet, "+
					"consectetur adipisicing elit, sed do eiusmod tempor incididunt " +
					"ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis " + "" +
					"nostrud exercitation ullamco laboris nisi ut aliquip ex ea " + "" +
					"commodo consequat. Duis aute irure dolor in reprehenderit in " + 
					"voluptate velit esse cillum dolore eu fugiat nulla pariatur. " +
					"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui " + 
					"officia deserunt mollit anim id est laborum"));
		
		
		return tokens;
	}
	
	
	

}
