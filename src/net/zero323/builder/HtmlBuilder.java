package net.zero323.builder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

public class HtmlBuilder implements Builder {
	
	protected Document document;
	
	@Override
	public void createDocument() {
		this.document = new Document();
	}

	@Override
	public Document getDocument() {
		return this.document;
	}

	@Override
	public Builder buildTitle(Token title) {
		this.document.setTitle("<html><head><title>" + title.getContent() +"</title></head>\n");
		return this;
	}

	@Override
	public Builder buildHeader(Token header) {
		switch (header.getType()) {
		case HEADER_H1:
			document.addContent("<h1>" + header.getContent() + "</h1>\n");
			break;
		case HEADER_H2:
			document.addContent("<h2>" + header.getContent() + "</h2>\n");
			break;
		case HEADER_H3:
			document.addContent("<h3>" + header.getContent() + "</h3>\n");
			break;
		case HEADER_H4:
			document.addContent("<h4>" + header.getContent() + "</h4>\n");
			break;
		}
		
		return this;
	}

	@Override
	public Builder buildList(Token list) {
		String str;
		BufferedReader sr = new BufferedReader(new StringReader(list.getContent()));
		document.addContent("<ul>\n");
		try {
			while ((str = sr.readLine()) != null) {
				document.addContent("<li>" + str + "</li>\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			document.addContent("</ul>\n");
		}
		return this;
	}

	@Override
	public Builder buildAuthor(Token author) {
		document.addAuthor("<meta name='author' content='" + author.getContent() +"/>\n");
		return this;
	}

	@Override
	public Builder buildParagaph(final Token paragraph) {
		document.addContent("<p>" + paragraph.getContent() + "</p>\n");
		return this;
	}

	@Override
	public Builder buildHeadStart() {
		document.addHeadStart("<html>\n<head>\n");
		return this;
	}

	@Override
	public Builder buildHeadStop() {
		document.addHeadStop("</head>\n");
		return this;
	}


	@Override
	public Builder buildFooter() {
		document.addFooter("</html>");
		return this;
		
	}
	

}
