package net.zero323.builder;

import java.util.ArrayList;

public class Director {
	private Builder builder;
	public void setBuilder(final Builder builder) {
		this.builder = builder;
	}
	
	public void constructDocument(ArrayList<Token> tokens) {
		builder.createDocument();
		builder.buildHeadStart();
		builder.buildHeadStop();
		builder.buildFooter();
		for(Token token: tokens) {
			switch (token.getType()) {
			case AUTHOR:
				builder.buildAuthor(token);
				break;
			case HEADER_H1:
				builder.buildHeader(token);
				break;
			case HEADER_H2:
				builder.buildHeader(token);
				break;
			case HEADER_H3:
				builder.buildHeader(token);
				break;
			case HEADER_H4:
				builder.buildHeader(token);
				break;
			case LIST:
				builder.buildList(token);
				break;
			case TITLE:
				builder.buildTitle(token);
				break;
			case PARGARPH:
				builder.buildParagaph(token);
				break;
			}
			
		}
	}
		
	public Document getDocument() {
		return builder.getDocument();
	}
			
	
}
