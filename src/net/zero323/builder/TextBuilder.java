package net.zero323.builder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TextBuilder implements Builder {
	
	protected Document document;
	private HashMap<TokenTypes, Integer> hm;
	//private TokenTypes lastHeader = TokenTypes.HEADER_H1;

	
	private TokenTypes lastHeaderLevel = TokenTypes.HEADER_H1;
	
	public void createDocument() {
		this.document = new Document();
		this.hm = new HashMap<TokenTypes, Integer>();
		hm.put(TokenTypes.HEADER_H1, new Integer(0));
		hm.put(TokenTypes.HEADER_H2, new Integer(0));
		hm.put(TokenTypes.HEADER_H3, new Integer(0));
		hm.put(TokenTypes.HEADER_H4, new Integer(0));
	}
	
	public Document getDocument() {
		return this.document;
	}

	@Override
	public Builder buildTitle(final Token title) {
		this.document.setTitle("Title: " + title.getContent() +"\n");
		return this;
	}

	@Override
	public Builder buildHeader(final Token header) {
		document.addContent(this.getHeaderString(header.getType()) + " " + header.getContent() + "\n");
		return this;
	}

	@Override
	public Builder buildList(final Token list) {
		String str;
		BufferedReader sr = new BufferedReader(new StringReader(list.getContent()));
		
		try {
			while ((str = sr.readLine()) != null) {
				document.addContent("*" + str + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return this;
	}

	@Override
	public Builder buildAuthor(final Token author) {
		document.addAuthor("Author: " + author.getContent() +"\n");
		return this;
	}

	@Override
	public Builder buildParagaph(final Token paragraph) {
		document.addContent(paragraph.getContent() + "\n");
		return this;
	}
	
	private String getHeaderString(TokenTypes tt) {
		for(Map.Entry<TokenTypes, Integer> entry: hm.entrySet()){
			TokenTypes key = entry.getKey();
			Integer value = entry.getValue();
			if(key.ordinal() > tt.ordinal()) entry.setValue(0);
			if(key.ordinal() == tt.ordinal()) entry.setValue(value + 1);
		}
		String header = hm.get(TokenTypes.HEADER_H1).toString() + "." +
		 				hm.get(TokenTypes.HEADER_H2).toString() + "." +
		 				hm.get(TokenTypes.HEADER_H3).toString() + "." +
		 				hm.get(TokenTypes.HEADER_H4).toString();
		return header;
					
	}

	@Override
	public Builder buildHeadStart() {
		document.addHeadStart("________________________________________________________________\n");
		return this;
	}

	@Override
	public Builder buildHeadStop() {
		document.addHeadStop("________________________________________________________________\n");
		return this;
	}


	@Override
	public Builder buildFooter() {
		document.addFooter("________________________________________________________________\n");
		return this;
		
	}
	
	
	
}
