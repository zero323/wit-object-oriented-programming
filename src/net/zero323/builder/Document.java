package net.zero323.builder;

import java.util.ArrayList;

public class Document {
	private String title = "";
	private String headStart = "";
	private String headStop = "";
	private String footer = "";
	private ArrayList<String> authors = new ArrayList<String>();
	private StringBuilder content = new StringBuilder();
	
	public void setTitle(final String title) {
		this.title = title;
	}
	public void addAuthor(final String author) {
		authors.add(author);
	}
	
	public void addHeadStart(final String headStart) {
		this.headStart = headStart;
	}
	
	public void addHeadStop(final String headStop) {
		this.headStop = headStop;
	}
	
	public void addFooter(final String footer) {
		this.footer = footer;
	}
	
	public void addContent(final String part) {
		this.content.append(part);
	}
	
	public String getContent(){
		String document = "";
		document += headStart;
		document = title;
		for (String author: authors) {
			document += author;
		}
		document += headStop;
		document += content;
		document += footer;
		return document.toString();
	}

}
