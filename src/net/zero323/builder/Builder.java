package net.zero323.builder;

import java.util.ArrayList;

/**
* Tutaj metody set/add
*
*/

public interface Builder {
	
	public void createDocument();
	
	public Document getDocument();
	
	public Builder buildTitle(Token title);
	
	public Builder buildHeader(Token header);
	
	public Builder buildList(Token list);
	
	public Builder buildAuthor(Token author);
	
	public Builder buildParagaph(Token paragraph);
	
	public Builder buildHeadStart();
	
	public Builder buildHeadStop();

	public Builder buildFooter();
	
}
