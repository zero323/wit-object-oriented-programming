package net.zero323.builder;
import java.util.ArrayList;

public class Token {
	private TokenTypes type;
	private String content; 
	public Token(final TokenTypes type, final String content) {
		this.type = type;
		this.content = content;
	}
	public TokenTypes getType() {
		return type;
	}
	public String getContent() {
		return content;
	}
	

}
