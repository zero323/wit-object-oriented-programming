package net.zero323.builder;

public enum TokenTypes {
	TITLE,
	HEADER_H1,
	HEADER_H2,
	HEADER_H3,
	HEADER_H4,
	AUTHOR,
	PARGARPH,
	LIST,
	HEAD_START,
	HEAD_STOP,
	FOOTER
}
