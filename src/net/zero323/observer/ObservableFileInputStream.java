package net.zero323.observer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ObservableFileInputStream extends FileInputStream implements Observable{
	ObservableFileInputStream(final String file) throws FileNotFoundException{
		super(file);
	}
}
