package net.zero323.plugin;

import java.util.Date;

public class SimpleTimeProvider implements TimeProvider {

	@Override
	public Date getTime() {
		return new Date();
	}

}
