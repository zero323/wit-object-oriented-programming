package net.zero323.plugin;

import java.util.Date;

public interface TimeProvider {
	Date getTime();
}
