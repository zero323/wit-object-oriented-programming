package net.zero323.plugin;

import java.util.Date;

public class FixedTimeProvider implements TimeProvider {
	private Date data;
	
	public FixedTimeProvider() {
		this.data = new Date(0);
	}
	
	@Override
	public Date getTime() {
			return data;
	}
	
	public void setTime(final Date data) {
		this.data = data;
	}

}
