package net.zero323.plugin;

import java.io.FileInputStream;
import java.util.Properties;

public class Registry {
	private static Properties prop;
	private static TimeProvider tp;
	
	public static TimeProvider getTimeProvider() {
		if (tp == null) {
			
			
				if (prop == null) {
					loadProperties();
				}
				String activeDateProvider = prop.getProperty("activeDateProvider");
				Object tpObject;
				
				try {
					tpObject = Class.forName(activeDateProvider).newInstance();
					tp = (TimeProvider)tpObject;
					
					
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return tp;
	}
	
	
	private static void loadProperties() {
		if (prop == null) {
			prop = new Properties();
			try {
		        FileInputStream inputStream = new FileInputStream("" +
		        		"/home/zero323/Workspace/eclipse/java/POJ/" +
		        		"src/net/zero323/plugin/registry.properties");
		        prop.load(inputStream);

				
			} catch (Exception e) {
				e.printStackTrace();
			}   
		}
	}

}

