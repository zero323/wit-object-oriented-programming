package net.zero323.money;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.Locale;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		BigDecimal bd1 = new BigDecimal("28.912");
		Money m1 = Money.createDontRound(bd1, Currency.getInstance("PLN"));
		bd1 = new BigDecimal("10");
		Money m2 = Money.createDontRound(bd1, Currency.getInstance("PLN"));
		System.out.println(m1);
		BigDecimal bd2 = new BigDecimal("2");
		m1 = m1.multiply(bd2);
		System.out.println(m1);
		m2 = m1.substract(m2);
		System.out.println(m1);
		System.out.println(m2);
		m2 = m1.negate();
		System.out.println(m2);
		
	}	

}
