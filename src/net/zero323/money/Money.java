package net.zero323.money;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

public final class Money implements Comparable<Money>{
	
	final BigDecimal amount;
	final Currency currency;
	final RoundingMode rm;
	final Boolean constantScale;
	
	private Money(final BigDecimal amount, final Currency currency,
			final RoundingMode rm, final Boolean constantScale) {
		if (amount == null || currency == null || rm == null || constantScale == null) {
			throw new IllegalArgumentException("None of the arguments can be null");
		}
		this.amount = amount;
		this.currency = currency;
		this.rm = rm;
		this.constantScale = constantScale;
	}
	
	/**
	 * createDontRound
	 * a) nie dba o liczbe miejsc w konstuktorze ani w trakcie operacji
	 * @param amount
	 * @param currency
	 * @param rm
	 * @return
	 */
	public static Money createDontRound(final BigDecimal amount, final Currency currency) {
		if (amount == null || currency == null) {
			throw new IllegalArgumentException("None of the arguments can be null");
		}
		return new Money(amount.setScale(amount.scale(), RoundingMode.UNNECESSARY), currency, RoundingMode.UNNECESSARY, false);
	}
	
	/**
	 * createRoundToDefaultScale
	 * b) zaokrąglenie do podanej wartości (w zależnosci od waluty)
	 * @param amount
	 * @param currency
	 * @param rm
	 * @return
	 */
	public static Money createRoundToDefaultScale(final BigDecimal amount, final Currency currency, final RoundingMode rm) {
		if (amount == null || currency == null || rm == null) {
			throw new IllegalArgumentException("None of the arguments can be null");
		}
		return new Money(amount.setScale(currency.getDefaultFractionDigits(), rm), currency, rm, true);
	}
	
	/**
	 * createCheckScale
	 * c) jeżeli liczba miejsc po przecinku niewłaściwa generujemy exception
	 * @param amount
	 * @param curr
	 * @param rm
	 * @return
	 */
	public static Money createCheckScale(final BigDecimal amount, final Currency currency, final RoundingMode rm) throws IllegalArgumentException {
		if (amount == null || currency == null || rm == null) {
			throw new IllegalArgumentException("None of the arguments can be null");
		}
		if (amount.scale() != currency.getDefaultFractionDigits()) {
			throw new IllegalArgumentException("Invalid number of fraction digits");
		} else {
			return new Money(amount.setScale(amount.scale(), rm), currency, rm, true);
	
		}
	}
	
	/**
	 * createRoundToGivenScale
	 * d) podajemy liczbę miejsc po przecinku, ale trzymamy się
	 * @param amount
	 * @param curr
	 * @param rm
	 * @param scale
	 * @return
	 */
	public static Money createRoundToGivenScale(final BigDecimal amount, final Currency currency, final RoundingMode rm, final int scale) {
		if (amount == null || currency == null || rm == null) {
			throw new IllegalArgumentException("None of the arguments can be null");
		}
		return new Money(amount.setScale(scale, rm), currency, rm, true);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.amount.toString() +
			" " + this.currency.getCurrencyCode() +
			", Rounding Mode: " + this.rm.toString() +
			", Scale: " + this.amount.scale() +
			", Constant Scale: " + this.constantScale.toString() +
			";" ;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(final Money other) {
		if (!this.isComparable(other)) {
			throw new IllegalArgumentException("Currencies not comparable");
		}
		return this.amount.subtract(other.amount).signum();
	}
	
	/**
	 * @param other
	 * @return
	 */
	public boolean lt(final Money other) {
		if (!this.isComparable(other)) {
			throw new IllegalArgumentException("Currencies not comparable");
		}
		if (this.compareTo(other) < 0 ) {
			return true;
		}
		return false;
	}
	
	/**
	 * @param other
	 * @return
	 */
	public boolean gt(final Money other) {
		if (!this.isComparable(other)) {
			throw new IllegalArgumentException("Currencies not comparable");
		}
		if (this.compareTo(other) > 0 ) {
			return true;
		}
		return false;
	}
	
	/**
	 * @return
	 */
	public boolean isZero() {
		if (this.amount.equals(new BigDecimal("0").setScale(this.amount.scale()))) {
			return true;
		}
		return false;
	}
	
	/**
	 * @param other
	 * @return
	 */
	public boolean isComparable(final Money other) {
		if (this == null || other == null) {
			return false;
		}
		if (this.currency != other.currency) {
			return false;
		}
		return true;
	}
	
	/**
	 * @param other
	 * @return
	 */
	public Money add(final Money other) {
		if (!this.isComparable(other)) {
			throw new IllegalArgumentException("Currencies not comparable");
		}	
		return new Money(this.amount.add(other.amount).setScale(this.amount.scale(), this.rm),
				this.currency, this.rm, this.constantScale);	
	}
	
	/**
	 * @param other
	 * @return
	 */
	public Money substract(final Money other) {
		if (!this.isComparable(other)) {
			throw new IllegalArgumentException("Currencies not comparable");
		}
		
		BigDecimal newAmount = this.amount.subtract(other.amount);
		
		if (this.constantScale == true) {
			return new Money(newAmount.setScale(this.amount.scale(), this.rm), this.currency, this.rm, this.constantScale);
		}
		return new Money(newAmount, this.currency, this.rm, this.constantScale);
	}
	
	/**
	 * @param multiplier
	 * @return
	 */
	public Money multiply(final BigDecimal multiplicand) {
		
		BigDecimal newAmount = this.amount.multiply(multiplicand);
		
		if (this.constantScale == true) {
			return new Money(newAmount.setScale(this.amount.scale(), this.rm), this.currency, this.rm, this.constantScale);
		}
		return new Money(newAmount, this.currency, this.rm, this.constantScale);
	}

	/**
	 * @return
	 */
	public Money negate() {
		return new Money(this.amount.negate(), this.currency, this.rm, this.constantScale);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result
				+ ((constantScale == null) ? 0 : constantScale.hashCode());
		result = prime * result
				+ ((currency == null) ? 0 : currency.toString().hashCode());
		result = prime * result + ((rm == null) ? 0 : rm.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Money other = (Money) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (constantScale == null) {
			if (other.constantScale != null)
				return false;
		} else if (!constantScale.equals(other.constantScale))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.toString().equals(other.currency.toString()))
			return false;
		if (rm == null) {
			if (other.rm != null)
				return false;
		} else if (!rm.equals(other.rm))
			return false;
		return true;
	}
	
	@Deprecated
	/**
	 * This method should be used only for debugging purposes
	 * @return
	 */
	public String amount() {
		return this.amount().toString();		
	}
	
	@Deprecated
	/**
	 * This method should be used only for debugging purposes
	 * @return
	 */
	public String currency() {
		return this.currency().toString();
	}
	
	@Deprecated
	/**
	 * This method should be used only for debugging purposes
	 * @return
	 */
	public String roundingMode() {
		return this.rm.toString();
	}
	
	@Deprecated
	/**
	 * This method should be used only for debugging purposes
	 * @return
	 */
	public String scale() {
		return Integer.toString(this.amount.scale());
	}
	
	@Deprecated
	/**
	 * This method should be used only for debugging purposes
	 * @return
	 */
	public String constantScale() {
		return this.constantScale().toString();
	}
	
}
