package net.zero323.money;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TestMoney {

	@Before
	public void setUp() throws Exception {
		
	}
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();


	@Test
	public void testCreateDontRound() {
		Money m1 = Money.createDontRound(
				new BigDecimal("28.789"), Currency.getInstance("PLN"));
		assertEquals("28.789 PLN, Rounding Mode: UNNECESSARY, Scale: 3, Constant Scale: false;",
				m1.toString());
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("None of the arguments can be null");
		Money m2 = Money.createDontRound(
			null, Currency.getInstance("PLN"));
		
	}
	
	@Test
	public void testCreateRoundToDefaultScale() {
		Money m1 = Money.createRoundToDefaultScale(
				new BigDecimal("28.789"), Currency.getInstance("PLN"), RoundingMode.UP);
		assertEquals("28.79 PLN, Rounding Mode: UP, Scale: 2, Constant Scale: true;",
				m1.toString());
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("None of the arguments can be null");
		Money m2 = Money.createRoundToDefaultScale(
				new BigDecimal("28.789"), null, RoundingMode.DOWN);
	}
	
	@Test
	public void testCreateCheckScale() {
		Money m1 = Money.createCheckScale(
				new BigDecimal("-9.99"), Currency.getInstance("EUR"), RoundingMode.UP);
		assertEquals("-9.99 EUR, Rounding Mode: UP, Scale: 2, Constant Scale: true;",
				m1.toString());
		
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("None of the arguments can be null");
		Money m2 = Money.createCheckScale(
				new BigDecimal("100"), Currency.getInstance("USD"), null);
		
		expectedException.expectMessage("Invalid number of fraction digits");
		Money m3 = Money.createCheckScale(
				new BigDecimal("100.91237912"), Currency.getInstance("EUR"), RoundingMode.HALF_DOWN);
	}
	

	@Test
	public void testCreateRoundToGivenScale() {
		Money m1 = Money.createRoundToGivenScale(
				new BigDecimal("2"), Currency.getInstance("JPY"), RoundingMode.UP, 1);
		assertEquals("2.0 JPY, Rounding Mode: UP, Scale: 1, Constant Scale: true;",
				m1.toString());
		
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("None of the arguments can be null");
		Money m2 = Money.createRoundToGivenScale(
				new BigDecimal("100.00"), Currency.getInstance("USD"), null, 9);
		
	}
	

	@Test
	public void testToString() {
		Money m1 = Money.createRoundToDefaultScale(
				new BigDecimal("2.55"), Currency.getInstance("JPY"), RoundingMode.HALF_EVEN);
		assertEquals("3 JPY, Rounding Mode: HALF_EVEN, Scale: 0, Constant Scale: true;",
				m1.toString());
	}
	

	@Test
	public void testCompareTo() {
		Money m1 = Money.createDontRound(
				new BigDecimal("2.55"), Currency.getInstance("EUR"));
		Money m2 = Money.createCheckScale(
				new BigDecimal("10.99"), Currency.getInstance("EUR"), RoundingMode.CEILING);
		Money m3 = Money.createRoundToDefaultScale(
				new BigDecimal("2.55"), Currency.getInstance("EUR"), RoundingMode.HALF_EVEN);
		Money m4 = Money.createRoundToDefaultScale(
				new BigDecimal("2.55"), Currency.getInstance("JPY"), RoundingMode.HALF_EVEN);
		
		assertEquals(-1, m1.compareTo(m2));
		assertEquals(1, m2.compareTo(m1));
		assertEquals(0, m1.compareTo(m1));
		assertEquals(0, m1.compareTo(m3));
		
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Currencies not comparable");
		
		m1.compareTo(m4);
		
	}
	
	@Test
	public void testLt() {
		Money m1 = Money.createDontRound(
				new BigDecimal("2.55"), Currency.getInstance("EUR"));
		Money m2 = Money.createCheckScale(
				new BigDecimal("10.99"), Currency.getInstance("EUR"), RoundingMode.FLOOR);
		Money m3 = Money.createRoundToDefaultScale(
				new BigDecimal("2.55"), Currency.getInstance("EUR"), RoundingMode.HALF_EVEN);
		Money m4 = Money.createRoundToDefaultScale(
				new BigDecimal("2.55"), Currency.getInstance("JPY"), RoundingMode.HALF_EVEN);
		
		assertTrue(m1.lt(m2));
		assertFalse(m2.lt(m1));
		assertFalse(m1.lt(m1));
		assertFalse(m1.lt(m3));
		
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Currencies not comparable");
		
		assertFalse(m1.lt(m4));
		

	}
	
	
	@Test
	public void testGt() {
		Money m1 = Money.createDontRound(
				new BigDecimal("2.55"), Currency.getInstance("EUR"));
		Money m2 = Money.createCheckScale(
				new BigDecimal("10.99"), Currency.getInstance("EUR"), RoundingMode.HALF_DOWN);
		Money m3 = Money.createRoundToDefaultScale(
				new BigDecimal("2.55"), Currency.getInstance("EUR"), RoundingMode.HALF_EVEN);
		Money m4 = Money.createRoundToDefaultScale(
				new BigDecimal("2.55"), Currency.getInstance("JPY"), RoundingMode.HALF_EVEN);
		
		assertFalse(m1.gt(m2));
		assertTrue(m2.gt(m1));
		assertFalse(m1.gt(m1));
		assertFalse(m1.gt(m3));
		
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Currencies not comparable");
		
		assertFalse(m1.gt(m4));
	}
	


	@Test
	public void testIsZero() {
		Money m1 = Money.createDontRound(
				new BigDecimal("0.00"), Currency.getInstance("EUR"));
		Money m2 = Money.createCheckScale(
				new BigDecimal("10.99"), Currency.getInstance("EUR"), RoundingMode.HALF_EVEN);
		
		assertTrue(m1.isZero());
		assertFalse(m2.isZero());
	}
	

	@Test
	public void testIsComparable() {
		Money m1 = Money.createDontRound(
				new BigDecimal("2.55"), Currency.getInstance("EUR"));
		Money m2 = Money.createCheckScale(
				new BigDecimal("10.99"), Currency.getInstance("EUR"), RoundingMode.UP);
		Money m3 = Money.createRoundToDefaultScale(
				new BigDecimal("2.55"), Currency.getInstance("JPY"), RoundingMode.DOWN);
		
		assertTrue(m1.isComparable(m2));
		assertTrue(m1.isComparable(m1));
		assertFalse(m1.isComparable(m3));
	}
	
	
	@Test
	public void testAdd() {
		Money m1 = Money.createDontRound(
				new BigDecimal("1231.320"), Currency.getInstance("EUR"));
		Money m2 = Money.createCheckScale(
				new BigDecimal("10.99"), Currency.getInstance("EUR"), RoundingMode.HALF_DOWN);
		Money m3 = Money.createRoundToDefaultScale(
				new BigDecimal("2.55"), Currency.getInstance("JPY"), RoundingMode.HALF_EVEN);
		
		assertEquals("1242.310 EUR, Rounding Mode: UNNECESSARY, Scale: 3, Constant Scale: false;",
				m1.add(m2).toString());
		assertEquals("1242.31 EUR, Rounding Mode: HALF_DOWN, Scale: 2, Constant Scale: true;",
				m2.add(m1).toString());
	
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Currencies not comparable");
		m1.add(m3);
	}
	
	

	@Test
	public void testSubstract() {
		Money m1 = Money.createDontRound(
				new BigDecimal("1231.320"), Currency.getInstance("EUR"));
		Money m2 = Money.createCheckScale(
				new BigDecimal("10.99"), Currency.getInstance("EUR"), RoundingMode.HALF_DOWN);
		Money m3 = Money.createRoundToDefaultScale(
				new BigDecimal("2.55"), Currency.getInstance("JPY"), RoundingMode.HALF_EVEN);
		
		assertEquals("1220.330 EUR, Rounding Mode: UNNECESSARY, Scale: 3, Constant Scale: false;",
				m1.substract(m2).toString());
		assertEquals("-1220.33 EUR, Rounding Mode: HALF_DOWN, Scale: 2, Constant Scale: true;",
				m2.substract(m1).toString());
	
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Currencies not comparable");
		m1.substract(m3);
	}
	
	
	@Test
	public void testMultiply() {
		Money m1 = Money.createDontRound(
				new BigDecimal("1231.320"), Currency.getInstance("EUR"));
		Money m2 = Money.createCheckScale(
				new BigDecimal("10.99"), Currency.getInstance("EUR"), RoundingMode.HALF_DOWN);
		
		BigDecimal bd1 = new BigDecimal("10.99");
		BigDecimal bd2 = new BigDecimal("-2.55");
		
		assertEquals("13532.20680 EUR, Rounding Mode: UNNECESSARY, Scale: 5, Constant Scale: false;",
				m1.multiply(bd1).toString());
		assertEquals("-28.02 EUR, Rounding Mode: HALF_DOWN, Scale: 2, Constant Scale: true;",
				m2.multiply(bd2).toString());
	}


	@Test
	public void testNegate() {
		Money m1 = Money.createDontRound(
				new BigDecimal("1231.320"), Currency.getInstance("EUR"));
		Money m2 = Money.createCheckScale(
				new BigDecimal("-10.99"), Currency.getInstance("EUR"), RoundingMode.HALF_DOWN);
		
		assertEquals("-1231.320 EUR, Rounding Mode: UNNECESSARY, Scale: 3, Constant Scale: false;", m1.negate().toString());
		assertEquals("10.99 EUR, Rounding Mode: HALF_DOWN, Scale: 2, Constant Scale: true;", m2.negate().toString());
		
	}

	
	@Test
	public void testEqualsObject() {
		Money m1 = Money.createDontRound(
				new BigDecimal("2.55"), Currency.getInstance("EUR"));
		Money m2 = Money.createCheckScale(
				new BigDecimal("10.99"), Currency.getInstance("EUR"), RoundingMode.HALF_UP);
		Money m3 = Money.createRoundToDefaultScale(
				new BigDecimal("2.55"), Currency.getInstance("EUR"), RoundingMode.HALF_EVEN);
		Money m4 = Money.createRoundToDefaultScale(
				new BigDecimal("2.55"), Currency.getInstance("EUR"), RoundingMode.HALF_EVEN);
		Money m5 = Money.createRoundToDefaultScale(
				new BigDecimal("2.55"), Currency.getInstance("JPY"), RoundingMode.HALF_DOWN);
		
		assertTrue(m1.equals(m1));
		assertTrue(m3.equals(m4));
		assertFalse(m2.equals(m5));
	}
	
}
